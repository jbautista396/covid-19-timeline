<h1 align="center">
COVID-19 Timeline
</h1>
<p align="center">
Made with React
</p>

[![Netlify Status](https://api.netlify.com/api/v1/badges/d8e8961c-c6c8-4840-9ac9-187b654a8652/deploy-status)](https://app.netlify.com/sites/covid19-timeline/deploys)[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://github.com/facebook/react/blob/master/LICENSE) [![npm version](https://img.shields.io/npm/v/react.svg?style=flat)](https://www.npmjs.com/package/react) 


View day-by-day stats & news events all in one place, alongside a linear graph from <a href="https://www.npmjs.com/package/victory-chart">VictoryCharts</a> and choropleth map via <a href="https://www.mapbox.com/">Mapbox</a>.

# Showcase
![Showcase](https://github.com/kthisisjosh/readme-assets/blob/master/covid19-timeline/Showcase.gif)

<details>
  <summary>More Screenshots</summary>
  <h3>The World</h3>
  <img src="https://github.com/kthisisjosh/readme-assets/blob/master/covid19-timeline/world.jpg" />

  <h3>Canada</h3>
  <img src="https://github.com/kthisisjosh/readme-assets/blob/master/covid19-timeline/canada.jpg" />

  <h3>United States of America</h3>
  <img src="https://github.com/kthisisjosh/readme-assets/blob/master/covid19-timeline/usa.jpg" />

</details>

# Installation
Install the dependencies

```sh
$ git clone https://github.com/kthisisjosh/COVID19-Timeline.git
$ npm install
```

Start from root path
```sh
$ npm start
```

# License

MIT