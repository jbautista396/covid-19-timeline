const data = [

    // FEBRUARY

    [
        {
            day: 1,
            articles: [

            ]
        },
        {
            day: 2,
            articles: [

            ]
        },
        {
            day: 3,
            articles: [

            ]
        },
        {
            day: 4,
            articles: [

            ]
        },
        {
            day: 5,
            articles: [

            ]
        },
        {
            day: 6,
            articles: [

            ]
        },
        {
            day: 7,
            articles: [

            ]
        },
        {
            day: 8,
            articles: [

            ]
        },
        {
            day: 9,
            articles: [

            ]
        },
        {
            day: 10,
            articles: [

            ]
        },

        // PLACEHOLDER CUTOFF \\

        {
            day: 11,
            articles: [
                {
                    content: "Coronavirus gets official name from WHO: COVID-19.",
                    img: "articleImages/virus.jpeg",
                    link: "https://www.nbcnews.com/health/health-news/coronavirus-gets-official-name-who-covid-19-n1134756",
                    credit: null
                }
            ]
        },
        {
            day: 12,
            articles: [

            ]
        },
        {
            day: 13,
            articles: [

            ]
        },
        {
            day: 14,
            articles: [
                
            ]
        },
        {
            day: 15,
            articles: [

            ]
        },
        {
            day: 16,
            articles: [

            ]
        },
        {
            day: 17,
            articles: [

            ]
        },
        {
            day: 18,
            articles: [

            ]
        },
        {
            day: 19,
            articles: [

            ]
        },
        {
            day: 20,
            articles: [

            ]
        },
        {
            day: 21,
            articles: [

            ]
        },
        {
            day: 22,
            articles: [

            ]
        },
        {
            day: 23,
            articles: [

            ]
        },
        {
            day: 24,
            articles: [
                {
                    content: "Dow closes down 1,000 points as coronavirus fears slam Wall Street",
                    img: "articleImages/dow.jpg",
                    link: "https://www.nbcnews.com/business/markets/dow-plunges-950-points-fears-coronavirus-will-tank-global-economic-n1141546",
                    credit: "RICHARD DREW / AP"
                }
            ]
        },
        {
            day: 25,
            articles: [

            ]
        },
        {
            day: 26,
            articles: [
                {
                    content: "California announced the first case in the U.S. with no clear source of exposure.",
                    img: "articleImages/virus.jpeg",
                    link: "https://www.nbcnews.com/health/health-news/after-mystery-coronavirus-case-california-health-officials-go-detective-mode-n1144231",
                    credit: null
                }
            ]
        },
        {
            day: 27,
            articles: [

            ]
        },
        {
            day: 28,
            articles: [

            ]
        },
        {
            day: 29,
            articles: [
                {
                    content: "President Donald Trump announced additional travel restrictions involving Iran and increased warnings about travel to Italy and South Korea.",
                    img: "articleImages/merlin_169792431_7dd8ddcb-2f35-47a9-b57e-024ed42a8edb-superJumbo.jpg",
                    link: "https://www.nbcnews.com/politics/donald-trump/coronavirus-trump-give-update-white-house-saturday-n1145921",
                    credit: "Mills/The New York Times"
                },
                {
                    content: "The first recorded coronavirus death in the U.S., a man in his 50s in Washington state.",
                    img: "articleImages/virus.jpeg",
                    link: "https://www.nbcnews.com/news/us-news/1st-coronavirus-death-u-s-officials-say-n1145931",
                    credit: null
                }
            ]
        },
    ],

    // MARCH

    [
        {
            day: 1,
            articles: [

            ]
        },
        {
            day: 2,
            articles: [

            ]
        },
        {
            day: 3,
            articles: [

            ]
        },
        {
            day: 4,
            articles: [

            ]
        },
        {
            day: 5,
            articles: [

            ]
        },
        {
            day: 6,
            articles: [
                {
                    content: "Trump signed an $8.3 billion emergency spending package to combat the coronavirus outbreak, as the number of global cases hit 100,000.",
                    img: "articleImages/merlin_169792431_7dd8ddcb-2f35-47a9-b57e-024ed42a8edb-superJumbo.jpg",
                    link: "https://www.nbcnews.com/politics/congress/senate-passes-8-3-billion-emergency-bill-combat-coronavirus-n1150521",
                    credit: "Mills/The New York Times"
                },
                {
                    content: "Vice President Mike Pence announced that 21 people aboard the Grand Princess, a cruise ship being held off the coast of California, tested positive for the coronavirus.",
                    img: "articleImages/merlin_169792431_7dd8ddcb-2f35-47a9-b57e-024ed42a8edb-superJumbo.jpg",
                    link: "https://www.nbcnews.com/politics/politics-news/21-people-grand-princess-cruise-ship-test-positive-coronavirus-pence-n1151936",
                    credit: "Mills/The New York Times"
                }
            ]
        },
        {
            day: 7,
            articles: [

            ]
        },
        {
            day: 8,
            articles: [
                
            ]
        },
        {
            day: 9,
            articles: [

            ]
        },
        {
            day: 10,
            articles: [

            ]
        },
        {
            day: 11,
            articles: [
                {
                    content: "The World Health Organization declared that the coronavirus outbreak “can be characterized as a pandemic,” which is defined as worldwide spread of a new disease for which most people do not have immunity.",
                    img: "articleImages/WHO.jpg",
                    link: "https://www.nbcnews.com/health/health-news/live-blog/coronavirus-updates-live-u-s-cases-top-1-000-spread-n1155241/ncrd1155646#liveBlogHeader",
                    credit: null
                },
                {
                    content: "The NBA suspended all basketball games after a player for the Utah Jazz preliminarily tested positive for COVID-19, the disease caused by the new coronavirus.",
                    img: "articleImages/GettyImages_487473351.0.webp",
                    link: "https://www.nbcnews.com/news/us-news/nba-suspends-remainder-season-coronavirus-spreads-n1156156",
                    credit: "Elsa/Getty Images"
                },
                {
                    content: "The Oscar-winning actor Tom Hanks and his wife, Rita Wilson, announced that they tested positive for the coronavirus. They announced on Instagram that they are being isolated and observed in Australia, where Hanks was in pre-production for a film.",
                    img: "articleImages/tomhanks.JPG",
                    link: "https://www.instagram.com/p/B9qBEyjJu4B/?utm_source=ig_embed",
                    credit: null
                },
                {
                    content: "Trump announced a new restriction on many foreign travelers from 26 countries in Europe, except for Ireland and the United Kingdom, for the next 30 days.",
                    img: "articleImages/merlin_169792431_7dd8ddcb-2f35-47a9-b57e-024ed42a8edb-superJumbo.jpg",
                    link: "https://www.nbcnews.com/politics/donald-trump/trump-make-prime-time-address-coronavirus-wednesday-night-n1155941",
                    credit: "Mills/The New York Times"
                },
            ]
        },
        {
            day: 12,
            articles: [
                {
                    content: "MLB announced that it will suspend spring training and delay the start of the regular baseball season by at least two weeks.",
                    img: null,
                    link: "https://www.nbcnews.com/health/health-news/live-blog/coronavirus-updates-live-u-s-slaps-travel-restrictions-most-europe-n1156266/ncrd1157221#liveBlogHeader",
                    credit: null
                },
                {
                    content: "The NHL announced that it will pause its hockey season. The league’s commissioner did not set an end date for the suspension.",
                    img: null,
                    link: null,
                    credit: null
                },
                {
                    content: "The NCAA canceled both the men’s and women’s college basketball tournaments, known as March Madness, after most conferences suspended their postseason tournaments.",
                    img: null,
                    link: "https://www.nbcnews.com/news/us-news/ncaa-cancels-march-madness-tournaments-n1157261",
                    credit: null
                }
            ]
        },
        {
            day: 13,
            articles: [
                {
                    content: "Trump declared a national state of emergency that could free up $50 billion to help fight the pandemic.",
                    img: null,
                    link: "https://www.nbcnews.com/politics/donald-trump/trump-hold-friday-afternoon-press-conference-coronavirus-n1157981",
                    credit: ""
                },
                {
                    content: "Trump tweeted that some cruise lines, including Princess Cruises, Norwegian and Royal Caribbean, will suspend outbound trips, at his request, for 30 days.",
                    img: null,
                    link: "https://twitter.com/realDonaldTrump/status/1238590988593086464?ref_src=twsrc%5Etfw",
                    credit: null
                },
                {
                    content: "States across the U.S., including Michigan, Pennsylvania and Maryland, announced plans to close schools over the coronavirus concerns.",
                    img: null,
                    link: null,
                    credit: null
                }
            ]
        },
        {
            day: 14,
            articles: [

            ]
        },
        {
            day: 15,
            articles: [
                {
                    content: "The White House announced that the European travel ban would be extended to include the U.K. and Ireland.",
                    img: null,
                    link: "https://www.nbcnews.com/health/health-news/live-blog/coronavirus-updates-live-house-approves-coronavirus-aid-bill-n1158821/ncrd1159006#liveBlogHeader",
                    credit: ""
                },
                {
                    content: "The number of confirmed cases in the U.S. surpassed 3,000, with New York, California and Washington recording the most confirmed cases. The national death toll rose to 61.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "Twenty-nine additional states, including New York, Massachusetts, South Carolina and Hawaii, announced school closures.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "The Centers for Disease Control and Prevention released guidelines recommending \"that for the next 8 weeks, organizers (whether groups or individuals) cancel or postpone in-person events that consist of 50 people or more throughout the United States.\"",
                    img: null,
                    link: null,
                    credit: ""
                },
            ]
        },
        {
            day: 16,
            articles: [
                {
                    content: "Wall Street plunged again, as the Dow Jones Industrial Average sank by 3,000 points and the S&P 500 and Nasdaq were down by around 12 percent by the closing bell.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "San Francisco imposed strict prohibitions on residents leaving their homes “except for essential needs,” becoming the first city in the U.S. to introduce such extreme measures in response to the pandemic.",
                    img: null,
                    link: "https://www.nbcnews.com/health/health-news/san-francisco-require-people-stay-home-except-essential-needs-n1160916",
                    credit: ""
                },
                {
                    content: "MLB announced that the start of the season will be pushed back eight weeks, per guidance from the CDC.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "President Trump advised all Americans to avoid gatherings of 10 or more people, to avoid going to bars and restaurants and to halt discretionary travel. The guidelines, from the administration’s coronavirus task force, will remain in effect for 15 days.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "U.S. researchers administered the first shot to the first person in a test of an experimental coronavirus vaccine. Even if the trials go well, health officials warned that a vaccine would not be widely available for at least 12 to 18 months.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "NASCAR announced it would postpone all races until at least the beginning of May.",
                    img: null,
                    link: null,
                    credit: ""
                }
            ]
        },
        {
            day: 17,
            articles: [
                {
                    content: "West Virginia, the last state in the U.S. without a confirmed coronavirus case, recorded its first. Confirmed cases across the country rose to more than 5,800 and the death toll surpassed 100.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "Maryland’s governor postponed the state’s primary election, a day after Ohio’s primary was called also off. Florida and Illinois and Arizona proceeded with their primaries.",
                    img: null,
                    link: "https://www.nbcnews.com/health/health-news/live-blog/coronavirus-updates-countries-across-world-step-restrictions-combat-crisis-n1161301/ncrd1161661#liveBlogHeader",
                    credit: ""
                }
            ]
        },
        {
            day: 18,
            articles: [
                {
                    content: "Canada and the U.S. agreed to close its borders to all “non-essential traffic.”",
                    img: null,
                    link: "https://www.nbcnews.com/politics/donald-trump/u-s-canada-agree-close-border-non-essential-traffic-trump-n1162796",
                    credit: ""
                },
                {
                    content: "The Trump administration suspended refugee admissions untill April 6 due to the coronavirus pandemic.",
                    img: null,
                    link: null,
                    credit: ""
                }
            ]
        },
        {
            day: 19,
            articles: [
                {
                    content: "President Trump signed a coronavirus aid bill into law. The Families First Coronavirus Response Act would provide free coronavirus testing and ensure paid emergency leave for those infected or caring for a family member with the illness, while also providing additional Medicaid funding, food assistance and unemployment benefits.",
                    img: null,
                    link: "https://www.nbcnews.com/politics/congress/senate-plans-vote-house-coronavirus-bill-wednesday-n1162851",
                    credit: ""
                },
                {
                    content: "The U.S. State Department raised the global travel advisory to Level 4: Do Not Travel, warning Americans against traveling internationally and for those abroad to consider returning immediately.",
                    img: null,
                    link: "https://www.nbcnews.com/news/us-news/coronavirus-state-department-raise-travel-warnings-americans-n1164021",
                    credit: ""
                },
                {
                    content: "California issued a statewide stay-at-home order asking residents to only leave the house if necessary.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "Connecticut delayed its presidential primary election to June.",
                    img: null,
                    link: null,
                    credit: ""
                }
            ]
        },
        {
            day: 20,
            articles: [
                {
                    content: "The U.S. announced plans to close the border with Mexico to all “nonessential travel.” Acting Homeland Security Secretary Chad Wolf said all immigrants who lack proper entry documentation will be turned away.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "Cases in California exceeded 1,000, more than doubling from only three days ago. The new figures came one day after Gov. Gavin Newsom issued a statewide “stay-at-home” order for the state’s 40 million residents.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "The Dow Jones Industrial Average sank by 916 points and the S&P 500 closed the day down 4.3 percent, marking its worst weekly performance since the 2008 financial crisis.",
                    img: null,
                    link: null,
                    credit: ""
                }
            ]
        },
        {
            day: 21,
            articles: [
                {
                    content: "Coronavirus cases in New York State, the hardest-hit in the U.S., surpassed 10,000.",
                    img: null,
                    link: "https://www.nbcnews.com/news/us-news/coronavirus-cases-new-york-state-now-top-10-000-n1165626",
                    credit: ""
                },
                {
                    content: "Gov. Phil Murphy of New Jersey issued a stay-at-home order for nearly all of the state's 9 million residents..",
                    img: null,
                    link: "https://www.nbcnews.com/news/us-news/new-jersey-gov-phil-murphy-issues-stay-home-order-nearly-n1165661",
                    credit: ""
                },
            ]
        },
        {
            day: 22,
            articles: [
                {
                    content: "President Trump announced that he would activate the federal National Guard to assist Washington, California and New York, three of the states hit hardest by the pandemic.",
                    img: null,
                    link: "https://www.nbcnews.com/politics/donald-trump/trump-says-he-s-activated-national-guard-new-york-california-n1166211",
                    credit: ""
                },
                {
                    content: "A vote to advance a massive coronavirus stimulus bill failed in the Senate. Democrats said they were dissatisfied with worker protections in the Republican-written bill.",
                    img: null,
                    link: null,
                    credit: "",
                },
                {
                    content: "Sen. Rand Paul, R-Ky., became the first known senator to test positive for coronavirus.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "Ohio Gov. Mike DeWine issued a new stay-at-home order for all non-essential workers.",
                    img: null,
                    link: null,
                    credit: ""
                }
            ]
        },
        {
            day: 23,
            articles: [
                {
                    content: "Convicted rapist and disgraced movie mogul Harvey Weinstein, 68, tested positive for coronavirus.",
                    img: null,
                    link: "https://www.nbcnews.com/news/us-news/harvey-weinstein-tests-positive-coronavirus-n1166436",
                    credit: ""
                },
                {
                    content: "Massachusetts Gov. Charlie Baker issued a stay-at-home advisory and ordered all non-essential businesses to close.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "Sen. Amy Klobuchar, D-Minn., said that her husband, law professor John Bessler, tested positive for coronavirus.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "Michigan Gov. Gretchen Whitmer signed a \"stay home, stay safe\" executive order to bar non-essential businesses from requiring employees to leave their homes.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "Virginia Gov. Ralph Northam announced that schools will remain closed for the rest of the school year.",
                    img: null,
                    link: "https://www.nbcwashington.com/news/local/virginia-officials-give-coronavirus-update-as-parents-await-update-on-school-closures/2251037/6",
                    credit: ""
                },
                {
                    content: "Stocks plunged again, after an emergency fiscal stimulus package was twice rejected by the Senate and a new round of cash injection from the Federal Reserve failed to stem market declines.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "Washington Gov. Jay Inslee issued a statewide \"stay home\" order, telling residents they must stay home unless they are \"pursuing an essential activity.\"",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "",
                    img: null,
                    link: null,
                    credit: ""
                }
            ]
        },
        {
            day: 24,
            articles: [
                {
                    content: "Coronavirus cases topped 50,000 in the U.S., with 637 deaths nationwide.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "President Trump approved disaster declarations for the states of Iowa and Louisiana. Iowa had 124 confirmed cases and reported its first death that day, while Louisiana had more than 1,300 cases and 46 deaths.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "Miami Mayor Francis Suarez, who tested positive for coronavirus and remained in quarantine, announced a shelter-in-place order for the city’s residents.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "Vermont Gov. Phil Scott issued a stay-at-home order for the state that included the closure of all non-essential businesses. The state had 95 confirmed coronavirus cases and 7 deaths.",
                    img: null,
                    link: null,
                    credit: ""
                },
            ]
        },
        {
            day: 25,
            articles: [
                {
                    content: "The Senate passed a massive $2 trillion stimulus package designed to ease the economic blow from the coronavirus pandemic. The bill was subsequently sent to the House.",
                    img: null,
                    link: "https://www.nbcnews.com/politics/congress/white-house-senate-reach-deal-massive-2-trillion-coronavirus-spending-n1168136",
                    credit: ""
                },
                {
                    content: "The WHO warned that the U.S. could become the global epicenter of the coronavirus pandemic. The country recorded 54,810 coronavirus cases, including 781 deaths.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "Deaths in the U.S. passed 1,000, as confirmed cases nationwide rose to more than 68,100.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "President Trump approved disaster declarations for Florida, Texas and North Carolina.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "The 74th Tony Awards and the 2020 Rock and Roll Hall of Fame event were postponed. A new date for the Tony’s was not announced, but the Rock and Roll Hall of Fame event will now take place on Nov. 7.",
                    img: null,
                    link: "https://t.co/KRLt2jJeK4",
                    credit: ""
                },
                {
                    content: "Minnesota Gov. Tim Walz issued a stay-at-home order for the state’s 5.6 million residents, other than those performing essential services.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "Idaho Gov. Brad Little announced a stay-at-home order for at least 21 days.",
                    img: null,
                    link: null,
                    credit: ""
                }
            ]
        },
        {
            day: 26,
            articles: [
                {
                    content: "U.S. coronavirus cases surpassed China. The U.S. reported at least 82,474, with more than 1,100 deaths, while China reported 81,961 cases and more than 3,000 deaths.",
                    img: null,
                    link: null,
                    credit: ""
                },
                {
                    content: "Wall Street rallied for the third straight day, despite record-breaking unemployment claims in the U.S.",
                    img: null,
                    link: "https://www.nbcnews.com/business/markets/stock-market-attempts-rally-amid-record-shattering-unemployment-figures-n1169396",
                    credit: ""
                },
                {
                    content: "The number of cases in California climbed past 3,000, while the number of deaths statewide stood at 65.",
                    img: null,
                    link: null,
                    credit: ""
                }
            ]
        },
        {
            day: 27,
            articles: [

            ]
        },
        {
            day: 28,
            articles: [

            ]
        },
        {
            day: 29,
            articles: [

            ]
        },
        {
            day: 30,
            articles: [

            ]
        },
        {
            day: 31,
            articles: [

            ]
        },
    ],


    // APRIL

    [
        {
            day: 1,
            articles: [

            ]
        },
        {
            day: 2,
            articles: [

            ]
        },
        {
            day: 3,
            articles: [

            ]
        },
        {
            day: 4,
            articles: [

            ]
        },
        {
            day: 5,
            articles: [

            ]
        },
        {
            day: 6,
            articles: [

            ]
        },
        {
            day: 7,
            articles: [

            ]
        },
        {
            day: 8,
            articles: [

            ]
        },
        {
            day: 9,
            articles: [

            ]
        },
        {
            day: 10,
            articles: [

            ]
        },
        {
            day: 11,
            articles: [

            ]
        },
        {
            day: 12,
            articles: [

            ]
        },
        {
            day: 13,
            articles: [

            ]
        },
        {
            day: 14,
            articles: [

            ]
        },
        {
            day: 15,
            articles: [

            ]
        },
        {
            day: 16,
            articles: [

            ]
        },
        {
            day: 17,
            articles: [

            ]
        },
        {
            day: 18,
            articles: [

            ]
        },
        {
            day: 19,
            articles: [

            ]
        },
        {
            day: 20,
            articles: [

            ]
        },
        {
            day: 21,
            articles: [

            ]
        },
        {
            day: 22,
            articles: [

            ]
        },
        {
            day: 23,
            articles: [

            ]
        },
        {
            day: 24,
            articles: [

            ]
        },
        {
            day: 25,
            articles: [

            ]
        },
        {
            day: 26,
            articles: [

            ]
        },
        {
            day: 27,
            articles: [

            ]
        },
        {
            day: 28,
            articles: [

            ]
        },
        {
            day: 29,
            articles: [

            ]
        },
        {
            day: 30,
            articles: [

            ]
        },
    ],

    // MAY

    [
        {
            day: 1,
            articles: [

            ]
        },
        {
            day: 2,
            articles: [

            ]
        },
        {
            day: 3,
            articles: [

            ]
        },
        {
            day: 4,
            articles: [

            ]
        },
        {
            day: 5,
            articles: [

            ]
        },
        {
            day: 6,
            articles: [

            ]
        },
        {
            day: 7,
            articles: [

            ]
        },
        {
            day: 8,
            articles: [

            ]
        },
        {
            day: 9,
            articles: [

            ]
        },
        {
            day: 10,
            articles: [

            ]
        },
        {
            day: 11,
            articles: [

            ]
        },
        {
            day: 12,
            articles: [

            ]
        },
        {
            day: 13,
            articles: [

            ]
        },
        {
            day: 14,
            articles: [

            ]
        },
        {
            day: 15,
            articles: [

            ]
        },
        {
            day: 16,
            articles: [

            ]
        },
        {
            day: 17,
            articles: [

            ]
        },
        {
            day: 18,
            articles: [

            ]
        },
        {
            day: 19,
            articles: [

            ]
        },
        {
            day: 20,
            articles: [

            ]
        },
        {
            day: 21,
            articles: [

            ]
        },
        {
            day: 22,
            articles: [

            ]
        },
        {
            day: 23,
            articles: [

            ]
        },
        {
            day: 24,
            articles: [

            ]
        },
        {
            day: 25,
            articles: [

            ]
        },
        {
            day: 26,
            articles: [

            ]
        },
        {
            day: 27,
            articles: [

            ]
        },
        {
            day: 28,
            articles: [

            ]
        },
        {
            day: 29,
            articles: [

            ]
        },
        {
            day: 30,
            articles: [

            ]
        },
        {
            day: 31,
            articles: [

            ]
        },
    ],

    // JUNE

    [
        {
            day: 1,
            articles: [

            ]
        },
        {
            day: 2,
            articles: [

            ]
        },
        {
            day: 3,
            articles: [

            ]
        },
        {
            day: 4,
            articles: [

            ]
        },
        {
            day: 5,
            articles: [

            ]
        },
        {
            day: 6,
            articles: [

            ]
        },
        {
            day: 7,
            articles: [

            ]
        },
        {
            day: 8,
            articles: [

            ]
        },
        {
            day: 9,
            articles: [

            ]
        },
        {
            day: 10,
            articles: [

            ]
        },
        {
            day: 11,
            articles: [

            ]
        },
        {
            day: 12,
            articles: [

            ]
        },
        {
            day: 13,
            articles: [

            ]
        },
        {
            day: 14,
            articles: [

            ]
        },
        {
            day: 15,
            articles: [

            ]
        },
        {
            day: 16,
            articles: [

            ]
        },
        {
            day: 17,
            articles: [

            ]
        },
        {
            day: 18,
            articles: [

            ]
        },
        {
            day: 19,
            articles: [

            ]
        },
        {
            day: 20,
            articles: [

            ]
        },
        {
            day: 21,
            articles: [

            ]
        },
        {
            day: 22,
            articles: [

            ]
        },
        {
            day: 23,
            articles: [

            ]
        },
        {
            day: 24,
            articles: [

            ]
        },
        {
            day: 25,
            articles: [

            ]
        },
        {
            day: 26,
            articles: [

            ]
        },
        {
            day: 27,
            articles: [

            ]
        },
        {
            day: 28,
            articles: [

            ]
        },
        {
            day: 29,
            articles: [

            ]
        },
        {
            day: 30,
            articles: [

            ]
        },
    ],
]

export default data;