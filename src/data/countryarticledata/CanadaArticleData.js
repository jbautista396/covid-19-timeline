const data = [

    // FEBRUARY

    [
        {
            day: 1,
            articles: [

            ]
        },
        {
            day: 2,
            articles: [

            ]
        },
        {
            day: 3,
            articles: [

            ]
        },
        {
            day: 4,
            articles: [

            ]
        },
        {
            day: 5,
            articles: [

            ]
        },
        {
            day: 6,
            articles: [

            ]
        },
        {
            day: 7,
            articles: [

            ]
        },
        {
            day: 8,
            articles: [

            ]
        },
        {
            day: 9,
            articles: [

            ]
        },
        {
            day: 10,
            articles: [

            ]
        },

        // PLACEHOLDER CUTOFF \\

        {
            day: 11,
            articles: [

            ]
        },
        {
            day: 12,
            articles: [
                {
                    content: "Ontario Confirms First Resolved Case of the 2019 Novel Coronavirus",
                    img: "articleImages/virus.jpeg",
                    link: "https://news.ontario.ca/mohltc/en/2020/02/ontario-confirms-resolved-case-of-the-2019-novel-coronavirus.html",
                    credit: ""
                }
            ]
        },
        {
            day: 13,
            articles: [
                {
                    content: " New coronavirus named The International Committee on Taxonomy of Viruses named the new coronavirus “severe acute respiratory syndrome-related coronavirus 2,” or SARS-CoV-2, while the World Health Organization named the disease caused by the virus “coronavirus disease 2019,” or COVID-2019.",
                    img: "articleImages/virus.jpeg",
                    link: "https://www.sciencemag.org/news/2020/02/bit-chaotic-christening-new-coronavirus-and-its-disease-name-create-confusion",
                    credit: "",
                },
                {
                    content: "Simpson: Ontario hospitals are not prepared for coronavirus. Hospital overcrowding represents a potential critical weakness in Canada’s preparedness for an outbreak of the new coronavirus, warned a Perth, Ontario emergency physician.",
                    img: "articleImages/china-health-novacyt.jpg",
                    link: "https://ottawacitizen.com/health/family-child/simpson-ontario-hospitals-are-not-prepared-for-coronavirus/",
                    credit: "KIM KYUNG HOON / REUTERS"
                }
            ]
        },
        {
            day: 14,
            articles: [
                {
                    content: "Demand for masks remains high. Some Canadian pharmacies are having trouble keeping up with high demand for face masks, despite health officials advising that they won’t necessarily protect against COVID-19.",
                    img: "articleImages/9f3f14511bb7415aadf25c38f3f2c130_18.jpg",
                    link: "https://globalnews.ca/news/6547409/coronavirus-face-masks-prices-demand/",
                    credit: "Kim Hong-Ji/Reuters"
                },
                {
                    content: "Volunteers supporting people to self-quarantine. A group of 260 volunteers in Edmonton are helping travelers from China to self-quarantine.",
                    img: "articleImages/covid-19-assessment-centre.webp",
                    link: "https://globalnews.ca/news/6540015/50-people-in-edmonton-in-self-imposed-quarantine-to-prevent-spreading-covid-19-coronavirus/",
                    credit: "(Craig Chivers/CBC)"
                }
            ]
        },
        {
            day: 15,
            articles: [

            ]
        },
        {
            day: 16,
            articles: [

            ]
        },
        {
            day: 17,
            articles: [

            ]
        },
        {
            day: 18,
            articles: [

            ]
        },
        {
            day: 19,
            articles: [
                {
                    content: "Research sheds light on transmission. A review published in the Journal of Travel Medicine suggested that the basic reproduction number or R0 for COVID-19 (that is, the number that indicates how quickly the virus spreads) is higher than the World Health Organization previously estimated",
                    img: "articleImages/iStock-1166396640-1024x683.jpg",
                    link: "https://www.medicalnewstoday.com/articles/coronavirus-may-spread-faster-than-who-estimate#Coronavirus-spreads-faster-than-SARS",
                    credit: "(medicalnewstoday)"
                },
                {
                    content: "Some deportations deferred. Canada is deferring deportations to Hubei province at the epicentre of China’s COVID-19 outbreak, but not to other parts of the country.",
                    img: "articleImages/webp.net-resizeimage-3.jpg",
                    link: "https://nationalpost.com/news/coronavirus-man-facing-deportation-to-china-from-canada-cites-raging-epidemic-as-reason-to-stay",
                    credit: "Cnsphoto via REUTERS"
                }
            ]
        },
        {
            day: 20,
            articles: [
                {
                    content: "Canada confirms its first case related to travel outside mainland China.",
                    img: "articleImages/masks.jpg",
                    link: "../../../public/articleImages/WHO.jpg",
                    credit: "Credit: SOPA Images/LightRocket/Getty"
                }
            ]
        },
        {
            day: 21,
            articles: [
                {
                    content: "Canadians repatriated from cruise ship arrive for quarantine in Cornwall, Ont.",
                    img: "articleImages/WHO.jpg",
                    link: "https://nationalpost.com/news/canada/canadians-repatriated-from-cruise-ship-arrive-for-quarantine-in-cornwall-ont",
                    credit: null
                }
            ]
        },
        {
            day: 22,
            articles: [

            ]
        },
        {
            day: 23,
            articles: [

            ]
        },
        {
            day: 24,
            articles: [

            ]
        },
        {
            day: 25,
            articles: [

            ]
        },
        {
            day: 26,
            articles: [

            ]
        },
        {
            day: 27,
            articles: [
                {
                    content: "Canada preparing for possible pandemic. Federal and provincial officials are taking stock of medical supplies and equipment they’d need to respond to a pandemic.",
                    img: "articleImages/iran-virus-outbreak-mideast.webp",
                    link: "https://www.cbc.ca/news/politics/inventory-medical-supplies-pandemic-coronavirus-1.5478793",
                    credit: "(Sajjad Safari/IIPA/The Associated Press)"
                },
                {
                    content: "Doctors urge stronger quarantine policy. A group of 23 Chinese-Canadian doctors signed an open letter urging a 14-day quarantine for everyone returning to Canada from China and other COVID-19 hotspots.",
                    img: "articleImages/eileen.JPG",
                    link: "https://nationalpost.com/news/toronto-area-doctors-urge-all-travellers-from-china-to-voluntarily-enter-two-week-quarantine",
                    credit: null
                },
                {
                    content: "Stock markets plunge over coronavirus fearsInternational financial markets suffered seven consecutive days of losses driven by coronavirus fears in the worst week since the 2008 global financial crisis.",
                    img: "articleImages/dow.jpg",
                    link: "https://www.cnn.com/2020/02/27/investing/asian-market-latest/index.html",
                    credit: "RICHARD DREW / AP"
                },
            ]
        },
        {
            day: 28,
            articles: [
                {
                    content: "WHO issues highest alert over COVID-19. The World Health Organization increased the risk assessment of COVID-19 across all countries to “very high” – it’s highest level of alert. ",
                    img: "articleImages/WHO.jpg",
                    link: "https://www.cnbc.com/2020/02/28/who-raises-risk-assessment-of-coronavirus-to-very-high-at-global-level.html",
                    credit: null
                },
                {
                    content: " New Canadian cases linked to Iran. Quebec reported its first presumptive COVID-19 case, a woman who returned to Montreal from Iran. If confirmed, it will be the 14th case in Canada, and the first outside of Ontario and British Columbia.",
                    img: "articleImages/fceb48e545d15cddcff1f699e8a713d4.jpg_1200x630.jpg",
                    link: "https://globalnews.ca/news/6606736/quebec-first-presumptive-coronavirus-case/",
                    credit: "Danielle McCann - Députée de Sanguinet | Facebook"
                },
            ]
        },
        {
            day: 29,
            articles: [
                {
                    content: "Woman who travelled from Iran is B.C.'s 8th case of COVID-19.",
                    img: "articleImages/province.jpg",
                    link: "https://vancouverisland.ctvnews.ca/woman-who-travelled-from-iran-is-b-c-s-8th-case-of-covid-19-1.4833403",
                    credit: null
                }
            ]
        },
    ],

    // MARCH

    [
        {
            day: 1,
            articles: [

            ]
        },
        {
            day: 2,
            articles: [
                {
                    content: " Canadians stockpiling supplies. Some stores have been cleared out of household disinfectants and personal hygiene products, in addition to face masks, as Canadians have begun preparing for potential quarantines.",
                    img: "articleImages/virus.jpeg",
                    link: "https://globalnews.ca/news/6617690/coronavirus-household-cleaners/",
                    credit: ""
                },
                {
                    content: "More Ontario cases linked to Iran, Egypt. Ontario reported seven new cases over the weekend and three new cases on Monday, all linked to Iran or Egypt.",
                    img: "articleImages/COVIDTest.jpg",
                    link: "https://toronto.ctvnews.ca/three-new-cases-of-covid-19-in-gta-bringing-ontario-total-to-18-1.4835139",
                    credit: "THE CANADIAN PRESS/Graham Hughes"
                },
            ]
        },
        {
            day: 3,
            articles: [
                {
                    content: "COVID-19 appears to be deadlier than flu. The mortality rate among people diagnosed with COVID-19 is 3.4%, according to the World Health Organization – higher than the seasonal flu fatality rate of less than 1% and the mortality rate of the Spanish flu, which killed 2.5% of people infected between 1918 and 1919.",
                    img: "articleImages/6b7a7eedf76077b2c84431b068b288b1206a5aff-16x9-x0y412w4000h2250.webp",
                    link: "https://7news.com.au/lifestyle/health-wellbeing/coronavirus-now-deadlier-than-flu-says-who-c-728779",
                    credit: "Credit: Getty"
                },
                {
                    content: " Canadians should prepare for disruptions to daily life. Canada’s Chief Public Health Officer Dr. Theresa Tam said communities should prepare for stronger public health measures to contain the spread of the new coronavirus, including closures of schools and workplaces and the suspension of large-scale gathering.",
                    img: "articleImages/2020-02-03T183049Z_250934987_RC26TE9GW1LV_RTRMADP_3_CHINA-HEALTH-CANADA_large.jfif",
                    link: "https://www.thechronicleherald.ca/news/canada/canadians-told-to-expect-more-measures-to-prevent-coronavirus-spread-418436/",
                    credit: "Blair Gable / Reuters"
                },
                {
                    content: "More travel warnings and cancellations. The government of Canada issued a travel advisory for Iran, warning against non-essential travel due to the spread of COVID-19 in the country.",
                    img: "articleImages/shutterstock_1183296613.jpg",
                    link: "https://dailyhive.com/vancouver/canada-travel-advisory-iran-coronavirus",
                    credit: "Tehran, Iran / Shutterstock"
                },
            ]
        },
        {
            day: 4,
            articles: [
                {
                    content: "Trudeau creates COVID-19 committee. Prime Minister Justin Trudeau announced the creation of a new Cabinet committee to focus on COVID-19.",
                    img: "articleImages/cabinet.JPG",
                    link: "https://globalnews.ca/news/6628805/coronavirus-trudeau-cabinet-committee/",
                    credit: "Global National"
                },
                {
                    content: "Military shifts to \"pre-pandemic planning\". Canada’s military has shifted to “pre-pandemic planning” mode and is preparing for an absentee rate among its ranks of about 25%.",
                    img: "articleImages/military.JPG",
                    link: "https://globalnews.ca/news/6631960/coronavirus-canadian-military-pandemic-planning/",
                    credit: "Global News"
                },
                {
                    content: "Potential vaccine may soon undergo trials. Trials of a potential vaccine by a company called Moderna may begin as early as April.",
                    img: "articleImages/2560.jpg",
                    link: "https://www.theguardian.com/commentisfree/2020/mar/04/market-coronavirus-vaccine-us-health-virus-pharmaceutical-business",
                    credit: "Illustration: Bill Bragg/The Guardian"
                },
            ]
        },
        {
            day: 5,
            articles: [
                {
                    content: "Canada confirms first community case of COVID-19. British Columbia announced its first case of apparent local community transmission, a woman with no recent travel history and no known contact with anyone infected with the virus.",
                    img: "articleImages/toronto.JPG",
                    link: "https://www.ctvnews.ca/health/canada-confirms-first-community-case-of-covid-19-here-s-what-that-means-1.4841249",
                    credit: null
                },
                {
                    content: "Alberta reports first presumptive case. Alberta’s chief medical officer confirms the province’s first presumptive case of COVID-19, a woman who travelled on the Grand Princess cruise ship that is now quarantined off the coast of California.",
                    img: "articleImages/77464322-covid-1924852-w.jpg",
                    link: "https://edmontonjournal.com/news/local-news/albertas-chief-medical-officer-of-health-assures-coronavirus-response-plan-is-solid-urges-public-to-plan",
                    credit: "ED KAISER / Postmedia"
                },
                {
                    content: "Ontario expands testing. Ontario’s public health agency is expanding testing for missed cases of COVID-19 at hospitals in six cities.",
                    img: "articleImages/coronavirus_test_kit.jpg",
                    link: "https://www.thestar.com/news/gta/2020/03/05/coronavirus-testing-ramps-up-as-ontario-searches-for-missed-cases.html",
                    credit: null
                },
                {
                    content: "Universities facing financial crunch. Canadian schools and universities that rely on fees from international students are assessing the possible financial impacts of COVID-19.",
                    img: "articleImages/png-0305n-internationalstudents-046-1.jpg",
                    link: "https://vancouversun.com/news/local-news/two-b-c-post-secondary-schools-close-over-covid-19-case",
                    credit: "JASON PAYNE / PNG"
                },
                {
                    content: "Rising financial anxieties related to COVID-19. An Ipsos poll of 10,000 adults in Canada and nine other countries conducted between Feb. 28 and 29 found that 37% of Canadians believe the spread of COVID-19 will affect their personal finances, up from 20% two weeks earlier.",
                    img: "articleImages/WHO.jpg",
                    link: "https://globalnews.ca/news/6630705/coronavirus-canada-personal-finance/",
                    credit: null
                },
            ]
        },
        {
            day: 6,
            articles: [
                {
                    content: "Doctors raise concern about preparedness: A group of 1,600 Edmonton doctors criticized Alberta’s handling of COVID-19 an hour before the province announced its first presumptive case.",
                    img: "articleImages/77441290-maz-w.jpg",
                    link: "https://edmonton.ctvnews.ca/covid-19-edmonton-doctors-say-alberta-isn-t-ready-1.4841359",
                    credit: "SHAUGHN BUTTS / Postmedia"
                },
                {
                    content: "Hospitals report thefts of medical supplies. British Columbia Health Minister Adrian Dix condemned alleged thefts of medical supplies from hospitals. At least two hospitals in the province reported that workers have had to stop people from trying to steal boxes of masks.",
                    img: "articleImages/21028522_web_200319-BPD-CanadaCoronaVirusWrap-COVIDMAR19_1.jpg",
                    link: "https://globalnews.ca/news/6638647/coronavirus-mask-theft/",
                    credit: "THE CANADIAN PRESS/Darryl Dyck"
                },
                {
                    content: "California cruise ship quarantined. Some 235 Canadians are among the 3,500 passengers quarantined on the Grand Princess cruise ship off the coast of California after a traveler from a previous voyage died from illness related to COVID-19 infection.",
                    img: "articleImages/imrs.webp",
                    link: "https://globalnews.ca/news/6639506/california-cruise-ship-virus-test-results/",
                    credit: "(California National Guard via Reuters)"
                },
            ]
        },
        {
            day: 7,
            articles: [

            ]
        },
        {
            day: 8,
            articles: [

            ]
        },
        {
            day: 9,
            articles: [
                {
                    content: "First Canadian death related to COVID-19. British Columbia announced Canada’s first death related to COVID-19. ",
                    img: "articleImages/bc-min-of-health-adrian-dix-and-bc-provincial-health-officer-dr.webp",
                    link: "https://globalnews.ca/news/6650774/bc-covid-coronavirus-update-monday/",
                    credit: "(MIke McArthur/CBC)"
                },
                {
                    content: "Public Health Agency of Canada warns against cruises. The Public Health Agency of Canada recommended that Canadian avoid all travel on cruise ships due to COVID-19. ",
                    img: "articleImages/WPAUNHZVAVDC7JC4NGBYCZEFWU.jpg",
                    link: "https://www.theglobeandmail.com/canada/article-ottawa-says-canadians-should-avoid-cruises-as-it-prepares-to-airlift/",
                    credit: "THE ASSOCIATED PRESS"
                },
            ]
        },
        {
            day: 10,
            articles: [
                {
                    content: "COVID-19 could markedly affect Canadians. A disease-transmission model developed by University of Toronto researchers predicts that the novel coronavirus could infect 35% of Canadians with modest controls, including finding and isolating about half of mild cases.",
                    img: "articleImages/coronavirus-1-1.png",
                    link: "https://nationalpost.com/news/canada/coronavirus-could-infect-35-to-70-per-cent-of-canadian-population-experts-say",
                    credit: "Kate Munsch/Reuters"
                },
                {
                    content: "Research supports 14-day quarantine. On average, people infected with COVID-19 start to show symptoms in 5.1 days, and 97.5% of those who develop symptoms will do so within 11.5 days, according to a study of 181 confirmed cases published in the Annals of Internal Medicine.",
                    img: "articleImages/m200504ff3.jpeg",
                    link: "https://annals.org/aim/fullarticle/2762808/incubation-period-coronavirus-disease-2019-covid-19-from-publicly-reported",
                    credit: null
                },
                {
                    content: "EU states ban travel, mass events. In the wake of Italy’s national lockdown, other European countries announced travel bans and cancelled mass gatherings.",
                    img: "articleImages/WHO.jpg",
                    link: "https://www.theguardian.com/world/2020/mar/10/coronavirus-several-eu-states-ban-mass-events-after-italian-lockdown",
                    credit: null
                },
            ]
        },
        {
            day: 11,
            articles: [
                {
                    content: "WHO declares global pandemic. The World Health Organization declared the worldwide outbreak of COVID-19 a pandemic. As of Wednesday, 114 countries have confirmed 118,000 cases. “We are deeply concerned both by the alarming levels of spread and severity, and by the alarming levels of inaction,” said Director General Tedros Adhanom Ghebreyesus. He explained that the situation is likely to worsen. ",
                    img: "articleImages/GettyImages_1206555087.7.webp",
                    link: "https://www.aljazeera.com/news/2020/03/eu-promises-takes-curb-coronavirus-live-updates-200310235816410.html",
                    credit: "Fabrice Coffrini/ AFP via Getty Images"
                },
                {
                    content: "Canada creates $1-billion response fund. Prime Minister Justin Trudeau announced a $1-billion fund for the domestic and global response to COVID-19.",
                    img: "articleImages/trudeau_presser.jpg",
                    link: "https://globalnews.ca/news/6659384/coronavirus-funding-trudeau/",
                    credit: "The Star"
                },
            ]
        },
        {
            day: 12,
            articles: [
                {
                    content: "Ontario closing public schools. The Ontario government ordered all public schools in the province to close between March 14 and April 5. ",
                    img: "articleImages/7KCO4J4MAFERRCJH5JP6UBOJLI.jpg",
                    link: "https://www.theglobeandmail.com/canada/article-ontario-to-close-all-public-schools-for-two-weeks-after-march-break/",
                    credit: "SEAN KILPATRICK/THE CANADIAN PRESS"
                },
                {
                    content: "Quebec asks sick people, returning travellers to self-isolate. Quebec is asking all people returning from abroad or who experience flu-like symptoms to self-isolate for 14 days.",
                    img: "articleImages/que-coronavirus-20200317-2.jpg",
                    link: "https://globalnews.ca/news/6665502/quebec-coronavirus-march-12/",
                    credit: "JACQUES BOISSINOT / THE CANADIAN PRESS"
                },
                {
                    content: "New Brunswick, Manitoba, Saskatchewan report first presumptive cases.",
                    img: "articleImages/virus.jpeg",
                    link: "https://globalnews.ca/news/6665896/manitoba-to-provide-coronavirus-update-thursday-morning/",
                    credit: null
                },
                {
                    content: "Trump address sows confusion. United States President Donald Trump announced a 30-day ban on travel to the United States from European countries and restrictions on cargo.",
                    img: "articleImages/200311211631-donald-trump-oval-office-march-11-2020-04-exlarge-169.jpg",
                    link: "https://www.cnn.com/2020/03/12/politics/donald-trump-coronavirus-europe-travel/index.html",
                    credit: null
                },
            ]
        },
        {
            day: 13,
            articles: [
                {
                    content: "Provinces ramp up response. Ontario, Alberta, British Columbia and Manitoba health officials recommended the immediate cancellation of all events over 250 people and urged against travel outside of Canada.",
                    img: "articleImages/chief.jpg",
                    link: "https://www.cp24.com/news/ontario-health-official-recommends-events-with-over-250-people-be-cancelled-1.4851858",
                    credit: "THE CANADIAN PRESS/Chris Young"
                },
                {
                    content: "Trudeau in isolation, working from home. Sophie Grégoire Trudeau, Prime Minister Justin Trudeau’s wife,  tested positive for COVID-19.",
                    img: "articleImages/merlin_170414520_17ee9779-b06e-436b-8a5a-d7b18f1266a1-superJumbo.jpg",
                    link: "https://www.cnn.com/2020/03/12/americas/sophie-gregoire-trudeau-coronavirus/index.html",
                    credit: "Justin Tang/The Canadian Press, via Associated Press"
                },
                {
                    content: "Shortages of protective equipment. Some Vancouver doctors are turning away patients because of a shortage of personal protective equipment for physicians and support staff.",
                    img: "articleImages/21024436_web1_200319-BPD-Mar19CovidIntlUpdate-USDrivethroughtest_1.jpg",
                    link: "https://bc.ctvnews.ca/dire-shortage-has-vancouver-doctors-turning-away-patients-seeking-covid-19-testing-1.4851403",
                    credit: "(AP Photo/Nam Y. Huh)"
                },
                {
                    content: "United States declares national emergency. United States President Donald Trump declared the coronavirus pandemic a national emergency, unlocking up to $50 billion for state and local response efforts.",
                    img: "articleImages/trumpnationalemergency.webp",
                    link: "https://www.ctvnews.ca/health/coronavirus/trump-declares-national-emergency-over-covid-19-1.4852051",
                    credit: "Bill O'Leary/The Washington Post"
                },
            ]
        },
        {
            day: 14,
            articles: [
                {
                    content: "Quebec, Ontario limit visits to seniors. Quebec’s Premier Francois Legault declared a provincial health emergency and asked people over age 70 to stay home until further notice.",
                    img: "articleImages/legault-covid-19-coronavirus-update.jfif",
                    link: "https://globalnews.ca/news/6677307/quebec-coronavirus-march14/",
                    credit: "(Sylvain Roy Roussel/Radio-Canada)"
                },
            ]
        },
        {
            day: 15,
            articles: [
                {
                    content: "First cases in PEI, NL, NS. Prince Edward Island reported its first confirmed case of COVID-19, a woman in her 50s who recently travelled on a cruise ship. ",
                    img: "articleImages/PE-09032020-COVID19-SN2_large.jfif",
                    link: "https://globalnews.ca/news/6677828/pei-first-confirmed-coronavirus-case/",
                    credit: "SaltWire file photo"
                },
            ]
        },
        {
            day: 16,
            articles: [
                {
                    content: "Canada closes borders. Prime Minister Justin Trudeau announced that Canada will close its borders to people who are not Canadian citizens or permanent residents.",
                    img: "articleImages/jtborderclose.JPG",
                    link: "https://nationalpost.com/news/canada/trudeau-travel-restrictions-ban-coronavirus-covid19-canada",
                    credit: "Reuters postmedia YT"
                },
                {
                    content: "WHO calls on countries to ramp up testing. World Health Organization Director-General Tedros Adhanom Ghebreyesus urged countries to increases testing for COVID-19, suggesting they screen everyone suspected of having the disease.",
                    img: "articleImages/WHO.jpg",
                    link: "https://nationalpost.com/health/canada-among-top-world-performers-in-testing-for-covid-19-despite-shortcomings",
                    credit: null
                },
            ]
        },
        {
            day: 17,
            articles: [
                {
                    content: "Ontario, PEI declare emergencies. Ontario confirmed its first death related to COVID-19, a 77-year-old man who tested positive for the virus after close contact with another positive case.",
                    img: "articleImages/virus.jpeg",
                    link: "https://globalnews.ca/news/6689357/ontario-coronavirus-death/?utm_source=site_banner",
                    credit: ""
                },
                {
                    content: "Trudeau may enact federal Emergency Act. Prime Minister Justin Trudeau may recall Parliament to pass legislation to support Canada’s response to COVID-19, including changes to employment insurance and potentially enacting some parts of the federal Emergencies Act.",
                    img: "articleImages/53C7RBDJEJEI7GZDURE43ZP4F4.jpg",
                    link: "https://www.theglobeandmail.com/politics/article-parliament-to-be-recalled-to-pass-covid-19-emergency-measures-trudeau/",
                    credit: "JUSTIN TANG/THE CANADIAN PRESS"
                },
                {
                    content: "Alberta, BC declare emergencies. Alberta declared a public health emergency and banned gatherings of more than 50 people, including weddings and funerals.",
                    img: "articleImages/Kenney_conference_March_17.png",
                    link: "https://www.stalberttoday.ca/local-news/alberta-declares-state-of-public-health-emergency-2-2174452",
                    credit: "YourAlberta YT"
                },
                {
                    content: "Human trials of vaccine and treatments underway  . The first human trial of a potential vaccine developed by Moderna Therapeutics started in the United States.",
                    img: "articleImages/_111140261_corona_whatyouneedtodo_title-nc.png",
                    link: "https://www.bbc.com/news/health-51906604",
                    credit: null
                },
            ]
        },
        {
            day: 18,
            articles: [
                {
                    content: "Canada-US border closes. Canada and the United States mutually agreed to temporarily close their shared border. According to Prime Minister Justin Trudeau, “these measures will last in place as long as we feel they need to last.”",
                    img: "articleImages/border-closure.webp",
                    link: "https://www.cbc.ca/news/politics/canada-us-border-deal-1.5501289",
                    credit: "(Ben Nelms/CBC)"
                },
                {
                    content: "PM announces $82 billion in economic aid. Prime Minister Justin Trudeau announced an additional $82 billion in relief measures to offset the economic impact of the coronavirus pandemic, including $27 billion in direct support for workers and businesses. The relief package represents more than 3% of Canada’s GDP.",
                    img: "articleImages/21032507_web1_trudeau-22937197.jpg",
                    link: "https://globalnews.ca/news/6694574/coronavirus-trudeau-economic-measures/",
                    credit: "THE CANADIAN PRESS/Adrian Wyld"
                },
                {
                    content: "No evidence COVID-19 was engineered. An analysis of the public genome sequence data for SARS-CoV-2, the virus that causes COVID-19, found no evidence that the virus was made in a laboratory or otherwise engineered, as has some have speculated online.",
                    img: "articleImages/200317175442_1_540x360.jpg",
                    link: "https://www.sciencedaily.com/releases/2020/03/200317175442.htm",
                    credit: "© pinkeyes / Adobe Stock"
                },
            ]
        },
        {
            day: 19,
            articles: [
                {
                    content: "NB declares emergency as cases climb across Canada. New Brunswick declared a public health emergency.",
                    img: "articleImages/Premier_Higgs_on_COVID-19_large.jpg",
                    link: "https://atlantic.ctvnews.ca/state-of-emergency-declared-in-new-brunswick-amid-covid-19-pandemic-1.4859993",
                    credit: "The Telegram"
                },
                {
                    content: "Trudeau calls on Canadians to donate blood. Prime Minister Justin Trudeau urged Canadians to donate blood, after Canadian Blood Services reported a spike in appointment cancellations by donors.",
                    img: "articleImages/6910814e-f0b7-45a8-9ce1-ea0a9f108f96.jpg",
                    link: "https://globalnews.ca/news/6701594/coronavirus-blood-donation-appointments/",
                    credit: "(Lethbridge News Now)"
                },
                {
                    content: " Italy’s death toll surpasses China as Wuhan reports no new cases. Italy has now recorded 3,405 deaths related to COVID-19, up 425 deaths from the day before, overtaking China’s total of 3,245. The total number of cases in the country increased 14.5% to 41,035.",
                    img: "articleImages/b190b6745fad4fbf99ab10fcb2b69782_18.jpg",
                    link: "https://www.aljazeera.com/news/2020/03/uk-schools-close-italy-covid-19-deaths-jump-live-updates-200318235116951.html",
                    credit: "[Christian Hartmann/Reuters]"
                },
            ]
        },
        {
            day: 20,
            articles: [
                {
                    content: "Global death toll passes 10,000. Worldwide, the death toll related to COVID-19 passed 10,000 and recorded infections exceeded 255,000.",
                    img: "articleImages/JH.JPG",
                    link: "https://coronavirus.jhu.edu/map.html",
                    credit: "John Hopkins University"
                },
                {
                    content: "Medical students take care of frontline workers’ kids. More than 100 University of Alberta medical students are volunteering to care for the children of frontline health care workers after the province cancelled in-person post-secondary classes.",
                    img: "articleImages/CORONAVIRUS-GERSON-MAR25-810x454.jpg",
                    link: "https://globalnews.ca/news/6707350/coronavirus-covid-19-alberta-university-students-child-care/",
                    credit: "(Paul Chiasson/CP)"
                },
                {
                    content: "EI applications and cannabis sales surge. More than 500,000 Canadians have filed for employment insurance in the past week, compared to under 27,000 during the same week last year.",
                    img: "articleImages/1000x-1.jpg",
                    link: "https://globalnews.ca/news/6707529/coronavirus-500000-canadians-employment-insurance/",
                    credit: "Cole Burston/Bloomberg"
                },
                {
                    content: "Canada will turn back asylum seekers. Canada will turn away asylum seekers coming from the United States; the countries will mutually close their borders at midnight on Mar. 20.",
                    img: "articleImages/justin_trudeau.jpg",
                    link: "https://www.thestar.com/politics/federal/2020/03/20/canada-to-turn-away-irregular-asylum-seekers-coming-from-the-us-justin-trudeau-says.html",
                    credit: "Adrian Wyld / The Canadian Press"
                },
            ]
        },
        {
            day: 21,
            articles: [
                {
                    content: "Ontario sees third death from COVID-19 as 58 new cases confirmed; Canada confirms over 1,300 cases; Canada-U.S. border closed to non-essential travel.",
                    img: "articleImages/border.jpg",
                    link: "https://www.thestar.com/news/gta/2020/03/21/us-canada-border-now-closed-to-all-but-essential-goods-and-workers.html",
                    credit: "PAUL SANCYA / AP"
                }
            ]
        },
        {
            day: 22,
            articles: [
                {
                    content: "Canada will not send athletes to Summer Olympics.",
                    img: "articleImages/olympics.jpg",
                    link: "https://www.thestar.com/news/city_hall/2020/03/22/canadian-toll-rises-to-1331-cases-19-deaths-clergy-go-digital-to-avoid-gatherings-conservative-leader-andrew-scheer-puts-down-partisan-sword-saying-there-really-isnt-much-philosophical-differen.html",
                    credit: "JAE C. HONG / AP"
                }
            ]
        },
        {
            day: 23,
            articles: [
                {
                    content: "New COVID-19 cases, deaths, and recoveries. Nova Scotia, which on Mar. 22 declared a state of emergency, reported 41 new cases, including in a child under 10.",
                    img: "articleImages/covid_large.jpg",
                    link: "https://www.capebretonpost.com/news/provincial/covid-19-tally-increases-to-41-cases-in-nova-scotia-428351/",
                    credit: "Capebreton Post - File"
                },
                {
                    content: "Thirteen healthcare workers test positive in Toronto. At least 13 healthcare workers have tested positive for COVID-19 in Toronto, where at least one hospital has ordered workers to re-use single-use equipment because of shortages.",
                    img: "articleImages/eileen.JPG",
                    link: "https://globalnews.ca/news/6717564/coronavirus-toronto-health-care-workers/",
                    credit: ""
                },
                {
                    content: "Social distancing enforcement and non-essential workplace closures. Prime Minister Justin Trudeau also urged all Canadians to “go home and stay home” and abide by social distancing instructions. The federal government launched an advertising campaign to educate people about the risks and will enforce the rules, if necessary.",
                    img: "articleImages/justin_trudeau.jpg",
                    link: "https://www.macleans.ca/news/canada/justin-trudeaus-daily-coronavirus-update-enough-is-enough-go-home-and-stay-home-full-transcript/",
                    credit: "Adrian Wyld / The Canadian Press"
                },
                {
                    content: "Ottawa invests in $192 million in vaccines, treatment. The federal government is providing $192 million to directly support the development and production of vaccines and treatments for COVID-19 in Canada.",
                    img: "articleImages/22974089-810x454.jpg",
                    link: "https://www.macleans.ca/news/canada/justin-trudeaus-daily-coronavirus-update-enough-is-enough-go-home-and-stay-home-full-transcript/",
                    credit: "(CP/Sean Kilpatrick)"
                },
                {
                    content: "Nearly half of Canadian COVID-19 cases due to community spread. The Public Health Agency of Canada confirmed that nearly half of all COVID-19 cases in Canada have been acquired through community spread.",
                    img: "articleImages/H5OC5DCBCBEX5BYFW2E6XFHT3M.jpg",
                    link: "https://www.theglobeandmail.com/canada/article-nearly-half-of-canadas-covid-19-cases-now-acquired-through-community/",
                    credit: "PAUL CHIASSON/THE CANADIAN PRESS"
                },
            ]
        },
        {
            day: 24,
            articles: [
                {
                    content: "Tensions rise over social distancing in Canada and internationally. Prime Minister Justin Trudeau warned again that the federal government will put \"much more stringent\" measures in place if people do not abide by social distancing recommendations.",
                    img: "articleImages/Trudeau-coronavirus-transcript-march-24-getty-images-810x454.jpg",
                    link: "https://www.macleans.ca/news/canada/justin-trudeau-canada-coronavirus-update-march-24-2020-full-transcript/",
                    credit: "(Dave Chan / AFP)"
                },
                {
                    content: " British Columbia offering drive-through testing to health workers. Doctors, nurses and other health care workers in Vancouver can now get drive-through testing to see if they have COVID-19.",
                    img: "articleImages/covid-19-self-assessment-centre.webp",
                    link: "https://www.cbc.ca/news/canada/british-columbia/bc-protecting-health-care-workers-covid-1.5507608",
                    credit: "(Ben Nelms/CBC)"
                },
                {
                    content: "Canadians returning home. More than a million Canadians and permanent residents returned home from abroad last week, according to the Canadian Border Services Agency. ",
                    img: "articleImages/air_canada.jpg",
                    link: "https://www.ctvnews.ca/health/coronavirus/more-than-a-million-canadians-and-permanent-residents-return-from-abroad-amid-covid-19-warnings-1.4865042",
                    credit: "RICK MADONIK / TORONTO STAR"
                },
            ]
        },
        {
            day: 25,
            articles: [
                {
                    content: "Senate passes emergency aid bill after overnight House compromise. The new law, which received royal assent, includes an $82-billion stimulus and a $2,000-a-month Canada Emergency Response Benefit.",
                    img: "articleImages/L4SEQ3T67FB3RI7CJNMY7FWPLU.jpg",
                    link: "https://www.ctvnews.ca/health/coronavirus/senate-passes-emergency-aid-bill-after-overnight-house-compromise-1.4867250",
                    credit: "ADRIAN WYLD/THE CANADIAN PRESS"
                },
                {
                    content: "Transplants, orthopaedic surgeries and routine testing delayed. Lung transplants are delayed except in cases of critical deterioration, which will affect patients across Canada.",
                    img: "articleImages/hospital.jpg",
                    link: "https://www.theglobeandmail.com/canada/article-transplant-patients-bumped-by-coronavirus-outbreak-face-anxiety-as/",
                    credit: null
                }
            ]
        },
        {
            day: 26,
            articles: [
                {
                    content: "Trump considering placing troops near Canada-US border during pandemic. United States President Donald Trump is considering placing soldiers along its border with Canada to prevent irregular crossings during the COVID-19 crisis, but Canada is resisting the move.",
                    img: "articleImages/BB11Kv0B.jfif",
                    link: "https://globalnews.ca/news/6735064/coronavirus-militarizing-canada-us-border/",
                    credit: "(AP Photo/Alex Brandon)"
                },
                {
                    content: "Testing delays and mask rationing in Ontario. Close to 11,000 people in Ontario are waiting for the results of tests for COVID-19. ",
                    img: "articleImages/cda-coronavirus-assessment-centre-20200313.webp",
                    link: "https://www.cbc.ca/news/canada/toronto/covid19-test-ontario-waiting-lab-results-coronavirus-1.5510644",
                    credit: "(Justin Tang/The Canadian Press)"
                },
                {
                    content: "BC bans resale of food, medical equipment, cleaning supplies. To stem a rising black market, British Columbia is banning the resale of food, protective equipment, medical supplies and cleaning products. ",
                    img: "articleImages/john-horgan.webp",
                    link: "https://www.cbc.ca/news/canada/british-columbia/covid-19-bc-state-of-emergency-orders-1.5510677",
                    credit: "(Chad Hipolito/Canadian Press)"
                },
                {
                    content: "More than 100 Quebecers hospitalized. Quebec reported a total 1,629 cases of COVID-19, up 290 from the day before. More than 100 people are now in hospital and 43 of them are in intensive care. Premier François Legault said Quebecers are facing “the biggest battle of our collective lives.” About half the province’s cases have come from Montreal, where 31 health workers have tested positive for COVID-19.",
                    img: "articleImages/covid-19-stock.webp",
                    link: "https://globalnews.ca/news/6734816/quebec-coronavirus-march-26/",
                    credit: "(Maggie MacPherson/CBC)"
                },
            ]
        },
        {
            day: 27,
            articles: [
                {
                    content: "Domestic air, rail travel barred for anyone showing COVID-19 symptoms: Trudeau",
                    img: "articleImages/viatrain.jpg",
                    link: "https://www.cp24.com/news/domestic-air-rail-travel-barred-for-anyone-showing-covid-19-symptoms-trudeau-1.4872535",
                    credit: "THE CANADIAN PRESS/Adrian Wyld"
                },
                {
                    content: "Ottawa announces wage subsidy for small, medium businesses. Canada’s unemployment rate could rise to 15% by the end of the year, according to a federal forecast based on the possibility of physical distancing lasting until August.",
                    img: "articleImages/justin_trudeau.jpg",
                    link: "https://globalnews.ca/news/6740289/pbo-report-coronavirus-pandemic/",
                    credit: "Adrian Wyld / The Canadian Press"
                },
                {
                    content: "Manitoba to deliver online cognitive behavioural training for pandemic anxiety. Manitobans experiencing anxiety because of the pandemic crisis will soon be able to access a 12-week cognitive behavioural counselling course online.",
                    img: "articleImages/empty-winnipeg-streets-covid-19.webp",
                    link: "https://www.cbc.ca/news/canada/manitoba/pallister-covid-19-update-1.5512111",
                    credit: "(Tyson Koschik/CBC)"
                }
            ]
        },
        {
            day: 28,
            articles: [

            ]
        },
        {
            day: 29,
            articles: [

            ]
        },
        {
            day: 30,
            articles: [

            ]
        },
        {
            day: 31,
            articles: [

            ]
        },
    ],


    // APRIL

    [
        {
            day: 1,
            articles: [

            ]
        },
        {
            day: 2,
            articles: [

            ]
        },
        {
            day: 3,
            articles: [

            ]
        },
        {
            day: 4,
            articles: [

            ]
        },
        {
            day: 5,
            articles: [

            ]
        },
        {
            day: 6,
            articles: [

            ]
        },
        {
            day: 7,
            articles: [

            ]
        },
        {
            day: 8,
            articles: [

            ]
        },
        {
            day: 9,
            articles: [

            ]
        },
        {
            day: 10,
            articles: [

            ]
        },
        {
            day: 11,
            articles: [

            ]
        },
        {
            day: 12,
            articles: [

            ]
        },
        {
            day: 13,
            articles: [

            ]
        },
        {
            day: 14,
            articles: [

            ]
        },
        {
            day: 15,
            articles: [

            ]
        },
        {
            day: 16,
            articles: [

            ]
        },
        {
            day: 17,
            articles: [

            ]
        },
        {
            day: 18,
            articles: [

            ]
        },
        {
            day: 19,
            articles: [

            ]
        },
        {
            day: 20,
            articles: [

            ]
        },
        {
            day: 21,
            articles: [

            ]
        },
        {
            day: 22,
            articles: [

            ]
        },
        {
            day: 23,
            articles: [

            ]
        },
        {
            day: 24,
            articles: [

            ]
        },
        {
            day: 25,
            articles: [

            ]
        },
        {
            day: 26,
            articles: [

            ]
        },
        {
            day: 27,
            articles: [

            ]
        },
        {
            day: 28,
            articles: [

            ]
        },
        {
            day: 29,
            articles: [

            ]
        },
        {
            day: 30,
            articles: [

            ]
        },
    ],

    // MAY

    [
        {
            day: 1,
            articles: [

            ]
        },
        {
            day: 2,
            articles: [

            ]
        },
        {
            day: 3,
            articles: [

            ]
        },
        {
            day: 4,
            articles: [

            ]
        },
        {
            day: 5,
            articles: [

            ]
        },
        {
            day: 6,
            articles: [

            ]
        },
        {
            day: 7,
            articles: [

            ]
        },
        {
            day: 8,
            articles: [

            ]
        },
        {
            day: 9,
            articles: [

            ]
        },
        {
            day: 10,
            articles: [

            ]
        },
        {
            day: 11,
            articles: [

            ]
        },
        {
            day: 12,
            articles: [

            ]
        },
        {
            day: 13,
            articles: [

            ]
        },
        {
            day: 14,
            articles: [

            ]
        },
        {
            day: 15,
            articles: [

            ]
        },
        {
            day: 16,
            articles: [

            ]
        },
        {
            day: 17,
            articles: [

            ]
        },
        {
            day: 18,
            articles: [

            ]
        },
        {
            day: 19,
            articles: [

            ]
        },
        {
            day: 20,
            articles: [

            ]
        },
        {
            day: 21,
            articles: [

            ]
        },
        {
            day: 22,
            articles: [

            ]
        },
        {
            day: 23,
            articles: [

            ]
        },
        {
            day: 24,
            articles: [

            ]
        },
        {
            day: 25,
            articles: [

            ]
        },
        {
            day: 26,
            articles: [

            ]
        },
        {
            day: 27,
            articles: [

            ]
        },
        {
            day: 28,
            articles: [

            ]
        },
        {
            day: 29,
            articles: [

            ]
        },
        {
            day: 30,
            articles: [

            ]
        },
        {
            day: 31,
            articles: [

            ]
        },
    ],

    // JUNE

    [
        {
            day: 1,
            articles: [

            ]
        },
        {
            day: 2,
            articles: [

            ]
        },
        {
            day: 3,
            articles: [

            ]
        },
        {
            day: 4,
            articles: [

            ]
        },
        {
            day: 5,
            articles: [

            ]
        },
        {
            day: 6,
            articles: [

            ]
        },
        {
            day: 7,
            articles: [

            ]
        },
        {
            day: 8,
            articles: [

            ]
        },
        {
            day: 9,
            articles: [

            ]
        },
        {
            day: 10,
            articles: [

            ]
        },
        {
            day: 11,
            articles: [

            ]
        },
        {
            day: 12,
            articles: [

            ]
        },
        {
            day: 13,
            articles: [

            ]
        },
        {
            day: 14,
            articles: [

            ]
        },
        {
            day: 15,
            articles: [

            ]
        },
        {
            day: 16,
            articles: [

            ]
        },
        {
            day: 17,
            articles: [

            ]
        },
        {
            day: 18,
            articles: [

            ]
        },
        {
            day: 19,
            articles: [

            ]
        },
        {
            day: 20,
            articles: [

            ]
        },
        {
            day: 21,
            articles: [

            ]
        },
        {
            day: 22,
            articles: [

            ]
        },
        {
            day: 23,
            articles: [

            ]
        },
        {
            day: 24,
            articles: [

            ]
        },
        {
            day: 25,
            articles: [

            ]
        },
        {
            day: 26,
            articles: [

            ]
        },
        {
            day: 27,
            articles: [

            ]
        },
        {
            day: 28,
            articles: [

            ]
        },
        {
            day: 29,
            articles: [

            ]
        },
        {
            day: 30,
            articles: [

            ]
        },
    ],
]

export default data;