const data = [
    {
        month: 1,
        daily: [
            {
                day: 11,
                cases: 12
            },
            {
                day: 12,
                cases: 12
            },
            {
                day: 13,
                cases: 13
            },
            {
                day: 14,
                cases: 13
            },
            {
                day: 15,
                cases: 13
            },
            {
                day: 16,
                cases: 13
            },
            {
                day: 17,
                cases: 13
            },
            {
                day: 18,
                cases: 13
            },
            {
                day: 19,
                cases: 13
            },
            {
                day: 20,
                cases: 13
            },
            {
                day: 21,
                cases: 15
            },
            {
                day: 22,
                cases: 15
            },
            {
                day: 23,
                cases: 15
            },
            {
                day: 24,
                cases: 15
            },
            {
                day: 25,
                cases: 15
            },
            {
                day: 26,
                cases: 15
            },
            {
                day: 27,
                cases: 16
            },
            {
                day: 28,
                cases: 16
            },
            {
                day: 29,
                cases: 24
            },
        ]
    },
    {
        month: 2,
        daily: [
            {
                day: 1,
                cases: 30
            },
            {
                day: 2,
                cases: 53
            },
            {
                day: 3,
                cases: 80
            },
            {
                day: 4,
                cases: 98
            },
            {
                day: 5,
                cases: 164
            },
            {
                day: 6,
                cases: 214
            },
            {
                day: 7,
                cases: 279
            },
            {
                day: 8,
                cases: 423
            },
            {
                day: 9,
                cases: 647
            },
            {
                day: 10,
                cases: 937
            },
            {
                day: 11,
                cases: 1215
            },
            {
                day: 12,
                cases: 1629
            },
            {
                day: 13,
                cases: 1896
            },
            {
                day: 14,
                cases: 2234
            },
            {
                day: 15,
                cases: 3487
            },
            {
                day: 16,
                cases: 4226
            },
            {
                day: 17,
                cases: 7038
            },
            {
                day: 18,
                cases: 10442
            },
            {
                day: 19,
                cases: 15219
            },
            {
                day: 20,
                cases: 18747
            },
            {
                day: 21,
                cases: 24583
            },
            {
                day: 22,
                cases: 33404
            },
            {
                day: 23,
                cases: 44183
            },
            {
                day: 24,
                cases: 54453
            },
            {
                day: 25,
                cases: 68440
            },
            {
                day: 26,
                cases: 85356
            },
            {
                day: 27,
                cases: 104126
            },
            {
                day: 28,
                cases: 123578
            },
            {
                day: 29,
                cases: 143491
            }
        ]
    }
]

export default data;