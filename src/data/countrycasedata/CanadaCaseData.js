const data = [
    {
        month: 1,
        daily: [
            {
                day: 11,
                cases: 3
            },
            {
                day: 12,
                cases: 4
            },
            {
                day: 13,
                cases: 4
            },
            {
                day: 14,
                cases: 5
            },
            {
                day: 15,
                cases: 8
            },
            {
                day: 16,
                cases: 8
            },
            {
                day: 17,
                cases: 8
            },
            {
                day: 18,
                cases: 8
            },
            {
                day: 19,
                cases: 8
            },
            {
                day: 20,
                cases: 8
            },
            {
                day: 21,
                cases: 9
            },
            {
                day: 22,
                cases: 9
            },
            {
                day: 23,
                cases: 10
            },
            {
                day: 24,
                cases: 11
            },
            {
                day: 25,
                cases: 11
            },
            {
                day: 26,
                cases: 12
            },
            {
                day: 27,
                cases: 14
            },
            {
                day: 28,
                cases: 15
            },
            {
                day: 29,
                cases: 20
            },
        ]
    },
    {
        month: 2,
        daily: [
            {
                day: 1,
                cases: 24
            },
            {
                day: 2,
                cases: 27
            },
            {
                day: 3,
                cases: 30
            },
            {
                day: 4,
                cases: 34
            },
            {
                day: 5,
                cases: 37
            },
            {
                day: 6,
                cases: 54
            },
            {
                day: 7,
                cases: 60
            },
            {
                day: 8,
                cases: 66
            },
            {
                day: 9,
                cases: 77
            },
            {
                day: 10,
                cases: 95
            },
            {
                day: 11,
                cases: 110
            },
            {
                day: 12,
                cases: 142
            },
            {
                day: 13,
                cases: 198
            },
            {
                day: 14,
                cases: 252
            },
            {
                day: 15,
                cases: 341
            },
            {
                day: 16,
                cases: 441
            },
            {
                day: 17,
                cases: 598
            },
            {
                day: 18,
                cases: 727
            },
            {
                day: 19,
                cases: 873
            },
            {
                day: 20,
                cases: 1087
            },
            {
                day: 21,
                cases: 1328
            },
            {
                day: 22,
                cases: 1470
            },
            {
                day: 23,
                cases: 2091
            },
            {
                day: 24,
                cases: 2792
            },
            {
                day: 25,
                cases: 3409
            },
            {
                day: 26,
                cases: 4043
            },
            {
                day: 27,
                cases: 4757
            },
            {
                day: 28,
                cases: 5655
            },
            {
                day: 29,
                cases: 6320
            }
        ]
    }
]

export default data;