const data = [
    {
        month: 1,
        daily: [
            {
                day: 11,
                cases: 45134
            },
            {
                day: 12,
                cases: 59287
            },
            {
                day: 13,
                cases: 64438
            },
            {
                day: 14,
                cases: 67100
            },
            {
                day: 15,
                cases: 69197
            },
            {
                day: 16,
                cases: 71329
            },
            {
                day: 17,
                cases: 73332
            },
            {
                day: 18,
                cases: 75184
            },
            {
                day: 19,
                cases: 75700
            },
            {
                day: 20,
                cases: 76677
            },
            {
                day: 21,
                cases: 77673
            },
            {
                day: 22,
                cases: 78651
            },
            {
                day: 23,
                cases: 79205
            },
            {
                day: 24,
                cases: 80087
            },
            {
                day: 25,
                cases: 80828
            },
            {
                day: 26,
                cases: 81820
            },
            {
                day: 27,
                cases: 83112
            },
            {
                day: 28,
                cases: 84615
            },
            {
                day: 29,
                cases: 86604
            },
        ]
    },
    {
        month: 2,
        daily: [
            {
                day: 1,
                cases: 88585
            },
            {
                day: 2,
                cases: 90443
            },
            {
                day: 3,
                cases: 93016
            },
            {
                day: 4,
                cases: 95314
            },
            {
                day: 5,
                cases: 98425
            },
            {
                day: 6,
                cases: 102050
            },
            {
                day: 7,
                cases: 106099
            },
            {
                day: 8,
                cases: 109991
            },
            {
                day: 9,
                cases: 114381
            },
            {
                day: 10,
                cases: 118948
            },
            {
                day: 11,
                cases: 126214
            },
            {
                day: 12,
                cases: 134509
            },
            {
                day: 13,
                cases: 145416
            },
            {
                day: 14,
                cases: 156475
            },
            {
                day: 15,
                cases: 169517
            },
            {
                day: 16,
                cases: 182414
            },
            {
                day: 17,
                cases: 198159
            },
            {
                day: 18,
                cases: 218744
            },
            {
                day: 19,
                cases: 244902
            },
            {
                day: 20,
                cases: 275550
            },
            {
                day: 21,
                cases: 304979
            },
            {
                day: 22,
                cases: 337459
            },
            {
                day: 23,
                cases: 378830
            },
            {
                day: 24,
                cases: 422574
            },
            {
                day: 25,
                cases: 471035
            },
            {
                day: 26,
                cases: 531868
            },
            {
                day: 27,
                cases: 596366
            },
            {
                day: 28,
                cases: 663127
            },
            {
                day: 29,
                cases: 723390
            }
        ]
    }
]

export default data;